#include "mainwindow.h"
#include <QApplication>
#include "addressbook.h"
#include "listview.h"
#include <QMap>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

   QMap<QString, QString> book;
   book.insert("John", "Johns Address");
   book.insert("Joseph", "Joseph, Reading");
   book.insert("Julian", "Winkipop, AUS");


  //AddressBook addressBook;
  //addressBook.show();
   MainWindow m(&book);
   m.show();

    return a.exec();
}
