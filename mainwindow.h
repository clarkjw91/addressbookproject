#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QMap>
class ListView;
class IndivView;
class QPushButton;

enum mainWindowMode {MAIN_LISTVIEW, MAIN_INDIVVIEW};

class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QMap<QString, QString> * book, QWidget *parent = nullptr);

public slots:
    void openListView();
    void openIndivView();
    void openIndivViewFromClick(QString);

private:
    void updateInterface(mainWindowMode);
    QPushButton * listBtn;
    QPushButton * indivBtn;

    ListView * listView;
    IndivView * indivView;
    mainWindowMode mode;
    QMap<QString, QString> * contacts;
};

#endif // MAINWINDOW_H
