#ifndef ADDRESSBOOK_H
#define ADDRESSBOOK_H

#include <QWidget>
#include <QMap>
class QLineEdit;
class QTextEdit;
class QPushButton;
class FindDialog;
class QListWidget;
class QGridLayout;

enum Mode { NAV_MODE, ADD_MODE, EDIT_MODE, DIALOG_OPEN};

class AddressBook : public QWidget
{
    Q_OBJECT

public:
    AddressBook(QWidget *parent = 0);

public slots:
    void addContact();
    void editContact();
    void removeContact();
    void findContact();
    void saveAddressBook();
    void loadAddressBook();
    void submitContact();
    void cancel();
    void nextContact();
    void prevContact();
    void openListView();


private:
    void updateInterface(Mode m);
    QLineEdit * nameLine;
    QTextEdit * addressText;

    QPushButton * listBtn;
    QPushButton * addBtn;
    QPushButton * editBtn;
    QPushButton * removeBtn;
    QPushButton * submitBtn;
    QPushButton * cancelBtn;
    QPushButton * nextBtn;
    QPushButton * prevBtn;
    QPushButton * findBtn;
    QPushButton * saveBtn;
    QPushButton * loadBtn;

    Mode currentMode;
    QMap<QString, QString> contacts;
    QString oldName;
    QString oldAddress;
    QString curAddressBookFileName;

    FindDialog * findBox;

    QGridLayout * mainLayout;



};

#endif // ADDRESSBOOK_H
