#include "mainwindow.h"
#include "listview.h"
#include "indivview.h"
#include <QVBoxLayout>
#include <QPushButton>


MainWindow::MainWindow(QMap<QString, QString> * book, QWidget *parent) : QWidget(parent), contacts(book)
{
    setMinimumSize(500, 500);
    mode = MAIN_LISTVIEW;
   listBtn = new QPushButton("List");
    indivBtn = new QPushButton("Detail View");
    listView = new ListView(contacts, this);
   indivView = new IndivView(contacts, this);
    QHBoxLayout * buttonLayoutA = new QHBoxLayout;
    buttonLayoutA->addWidget(listBtn);
    buttonLayoutA->addWidget(indivBtn);

    QVBoxLayout * mainLayout = new QVBoxLayout;
    mainLayout->addLayout(buttonLayoutA);
    mainLayout->addWidget(listView);
   mainLayout->addWidget(indivView);
    setLayout(mainLayout);
    updateInterface(MAIN_LISTVIEW);

    connect(listBtn, SIGNAL(clicked()), this, SLOT(openListView()));
    connect(indivBtn, SIGNAL(clicked()), this, SLOT(openIndivView()));
    connect(listView, SIGNAL(itemDoubleClicked(QString)), this, SLOT(openIndivViewFromClick(QString)));

}

void MainWindow::openListView(){
     listView->updateList();
    QString selName = indivView->getCurrentName();
    listView->setCurrentSelection(selName);
    updateInterface(MAIN_LISTVIEW);

}

void MainWindow::openIndivView(){
    QString selName = listView->getCurrentSelection();
    if(!selName.isEmpty())
    indivView->setCurrentName(selName);
    else {
      QMap<QString, QString>::iterator i = contacts->begin();
      indivView->setCurrentName(i.key());
    }


    updateInterface(MAIN_INDIVVIEW);
}

void MainWindow::openIndivViewFromClick(QString name){
     indivView->setCurrentName(name);
      updateInterface(MAIN_INDIVVIEW);
}

void MainWindow::updateInterface(mainWindowMode m){
    switch(m){
    case MAIN_LISTVIEW:{
        mode = m;
       indivView->hide();
       listView->show();
        break;
    }
    case MAIN_INDIVVIEW:{
        mode = m;
        listView->hide();
        indivView->show();
        break;
    }
    }
}
