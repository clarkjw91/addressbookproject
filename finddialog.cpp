#include "finddialog.h"
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QMessageBox>

FindDialog::FindDialog(QWidget * parent) : QDialog(parent)
{
 findBtn = new QPushButton("Find");
 lineEdit = new QLineEdit;

 QHBoxLayout * objLayout = new QHBoxLayout;
 objLayout->addWidget(lineEdit);
 objLayout->addWidget(findBtn);

 setLayout(objLayout);
 connect(findBtn, SIGNAL(clicked()), this, SLOT(findBtnClicked()));

}
void FindDialog::findBtnClicked(){
    QString searchStr = lineEdit->text();

    if(searchStr.isEmpty()){
        QMessageBox::information(this, tr("Empty Field"), tr("Please enter a name!"));
        return;
    }else {
        findText = searchStr;
        accept();
        lineEdit->clear();
        hide();
    }
}
