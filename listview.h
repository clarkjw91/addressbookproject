#ifndef LISTVIEW_H
#define LISTVIEW_H

#include <QWidget>
#include <QMap>
class QListWidget;
class QModelIndex;
class QListWidgetItem;

class ListView : public QWidget
{
    Q_OBJECT
public:
    explicit ListView(QMap<QString, QString> * contacts, QWidget *parent = nullptr);
    void updateList();
    void setCurrentSelection(QString name);
    QString getCurrentSelection();
signals:
     void itemDoubleClicked(QString);
public slots:
    void itemDoubleClicked(QListWidgetItem*);

private:
    QListWidget * listView;
    QMap<QString, QString> * contacts;

};

#endif // LISTVIEW_H
