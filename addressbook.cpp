#include "addressbook.h"
#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include <QListWidget>
#include <iostream>
#include "finddialog.h"

AddressBook::AddressBook(QWidget * parent) : QWidget(parent)
{

    //Demo Contacts
    contacts.insert("John", "Johns Address");
    contacts.insert("Joseph", "Joseph, Reading");
    contacts.insert("Julian", "Winkipop, AUS");

    QLabel * nameLabel = new QLabel("name: ");
    nameLine = new QLineEdit;
    QLabel * addressLabel = new QLabel("Address: ");
    addressText = new QTextEdit;
    listBtn = new QPushButton("View List");
    addBtn = new QPushButton("Add");
    editBtn = new QPushButton("Edit");
    removeBtn = new QPushButton("Remove");
    findBtn = new QPushButton("Find");
    saveBtn = new QPushButton("Save");
    loadBtn = new QPushButton("Load");
    submitBtn = new QPushButton("Submit");
    cancelBtn = new QPushButton("Cancel");
    nextBtn = new QPushButton("Next");
    prevBtn = new QPushButton("Prev");

    findBox = new FindDialog;




    QVBoxLayout * buttonLayoutA = new QVBoxLayout;
    buttonLayoutA->addWidget(listBtn);
    buttonLayoutA->addWidget(addBtn, Qt::AlignTop);
    buttonLayoutA->addWidget(editBtn);
    buttonLayoutA->addWidget(removeBtn);
    buttonLayoutA->addWidget(findBtn);
    buttonLayoutA->addWidget(saveBtn);
    buttonLayoutA->addWidget(loadBtn);
    buttonLayoutA->addWidget(submitBtn);
    buttonLayoutA->addWidget(cancelBtn);
    buttonLayoutA->addStretch();

    QVBoxLayout * buttonLayoutB = new QVBoxLayout;
    buttonLayoutB->addWidget(prevBtn);
    buttonLayoutB->addWidget(nextBtn);

    mainLayout = new QGridLayout;
    mainLayout->addWidget(nameLabel, 0, 0);
    mainLayout->addWidget(nameLine, 0, 1);
    mainLayout->addWidget(addressLabel, 1, 0, Qt::AlignTop);
    mainLayout->addWidget(addressText, 1, 1);
    mainLayout->addLayout(buttonLayoutA, 1, 2);
    mainLayout->addLayout(buttonLayoutB, 2,1);
    //setLayout(mainLayout);
    setWindowTitle("Simple Address Book");
    updateInterface(NAV_MODE);

    //set Demo Contacts
    QMap<QString, QString>::iterator i = contacts.begin();
    nameLine->setText(i.key());
    addressText->setText(i.value());
    setLayout(mainLayout);
/*
    connect(addBtn, SIGNAL(clicked()), this, SLOT(addContact()));
    connect(submitBtn, SIGNAL(clicked()), this, SLOT(submitContact()));
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(cancel()));
    connect(nextBtn, SIGNAL(clicked()), this, SLOT(nextContact()));
    connect(prevBtn, SIGNAL(clicked()), this, SLOT(prevContact()));
    connect(editBtn, SIGNAL(clicked()), this, SLOT(editContact()));
    connect(removeBtn, SIGNAL(clicked()), this, SLOT(removeContact()));
    connect(findBtn, SIGNAL(clicked()), this, SLOT(findContact()));
    connect(saveBtn, SIGNAL(clicked()), this, SLOT(saveAddressBook()));
    connect(loadBtn, SIGNAL(clicked()), this, SLOT(loadAddressBook()));
    connect(listBtn, SIGNAL(clicked()), this, SLOT(openListView()));

*/}

void AddressBook::addContact(){
    updateInterface(ADD_MODE);
    oldName = nameLine->text();
    oldAddress = addressText->toPlainText();

    nameLine->clear();
    addressText->clear();
}

void AddressBook::editContact(){
    updateInterface(EDIT_MODE);
    oldName = nameLine->text();
    oldAddress = addressText->toPlainText();

}
void AddressBook::removeContact(){
    QString name = nameLine->text();
    if(contacts.contains(name)){
        int confirmation = QMessageBox::question(this,
                           tr("Confirm Remove!"),
                           tr("Are you sure you want to remove \"%1\" ?").arg(name),
                            QMessageBox::Yes | QMessageBox::No);

        if(confirmation == QMessageBox::Yes){
            prevContact();
         contacts.remove(name);
         QMessageBox::information(this, tr("Remove Successful"),
                                  tr("\"%1\" has been removed!").arg(name));
        }
    }
    if(contacts.isEmpty()){
        nameLine->clear();
        addressText->clear();
    }
    updateInterface(NAV_MODE);
}

void AddressBook::findContact(){
    findBox->show();
    updateInterface(DIALOG_OPEN);
    if(findBox->exec() == QDialog::Accepted){
        QString name = findBox->getFindText();
        if(contacts.contains(name)){
            nameLine->setText(name);
            addressText->setText(contacts.value(name));
        }else{
            QMessageBox::information(this, tr("Contact not found!"), tr("Sorry \" %1 \" could not be found!").arg(name));
            updateInterface(NAV_MODE);
            return;
        }
    }
    updateInterface(NAV_MODE);
}

void AddressBook::saveAddressBook(){
    updateInterface(DIALOG_OPEN);
    curAddressBookFileName = QFileDialog::getSaveFileName(this, tr("Save Address Book"),
                                          "", tr("Address Book (*abk);;All Files(*)"));
    if(curAddressBookFileName.isEmpty()){
        updateInterface(NAV_MODE);
        return;
    }else{
        QFile file(curAddressBookFileName);
        if(!file.open(QIODevice::WriteOnly)){
            QMessageBox::information(this, tr("Unable to open file"), file.errorString());
            updateInterface(NAV_MODE);
            return;
        }else{
            QDataStream out(&file);
            out.setVersion(QDataStream::Qt_DefaultCompiledVersion);
            out << contacts;
        }
    }

    updateInterface(NAV_MODE);
}

void AddressBook::loadAddressBook(){
    updateInterface(DIALOG_OPEN);
    curAddressBookFileName = QFileDialog::getOpenFileName(this, tr("Open Address Book"), "",
                                                        tr("Address Book (*abk);;All Files (*)"));

    if(curAddressBookFileName.isEmpty()){
        updateInterface(NAV_MODE);
        return;
    }else{
        QFile file(curAddressBookFileName);
        if(!file.open(QIODevice::ReadOnly)){
            QMessageBox::information(this, tr("Unabled to open file"),
                                     file.errorString());
            updateInterface(NAV_MODE);
            return;
        }

        QDataStream in(&file);
        in.setVersion(QDataStream::Qt_DefaultCompiledVersion);
        contacts.empty();
        in >> contacts;
        if(contacts.isEmpty()){
            QMessageBox::information(this, tr("No Contacts in File"), "This File contains no contacts");
        }else{
            QMap<QString, QString>::iterator i = contacts.begin();
            nameLine->setText(i.key());
            addressText->setText(i.value());
        }
    }
    updateInterface(NAV_MODE);

}

void AddressBook::submitContact(){
    QString name = nameLine->text();
    QString address = addressText->toPlainText();

    if(name.isEmpty() || address.isEmpty()){
        QMessageBox::information(this, "Empty Field", "Please add a name and address");
        return;
    }
    switch(currentMode){
    case ADD_MODE:{
        if(!contacts.contains(name)){
            contacts.insert(name, address);
            QMessageBox::information(this, "Add Successful", tr("\"%1\" has been added!").arg(name));
        }else {
            QMessageBox::information(this, "Add Failed", tr("%1 already exists").arg(name));
            return;
        }
       break;
     }
    case EDIT_MODE:{
        if(oldName == name){
            contacts[name] = address;
             QMessageBox::information(this, "Edit Successful", tr("\"%1\" has been updated!").arg(name));
        }else{
            if(!contacts.contains(name)){
                contacts.insert(name, address);
                contacts.remove(oldName);
                QMessageBox::information(this, "Edit Successful", tr("\"%1\" has been updated with \"%2\"!").arg(oldName, name));
            }else {
                QMessageBox::information(this, "Edit Failed", tr("%1 already exists as another contact").arg(name));
                return;
            }
        }
        break;
     }
     default :
        std::cerr << "Invalid Enumeration!\n";
    }

    if(contacts.isEmpty()){
       nameLine->clear();
       addressText->clear();

    }
    updateInterface(NAV_MODE);
}
void AddressBook::cancel(){
    nameLine->setText(oldName);
    nameLine->setReadOnly(true);
    addressText->setText(oldAddress);
    addressText->setReadOnly(true);
    updateInterface(NAV_MODE);

}

void AddressBook::nextContact(){
    QString name = nameLine->text();
    QMap<QString, QString>::iterator i = contacts.find(name);

    if(i != contacts.end())
        ++i;
    if(i == contacts.end())
        i = contacts.begin();

    nameLine->setText(i.key());
    addressText->setText(i.value());
}

void AddressBook::prevContact(){
   QString name = nameLine->text();
    QMap<QString, QString>::iterator i = contacts.find(name);

    if(i == contacts.end()){
        nameLine->clear();
        addressText->clear();
        return;
    }

    if(i == contacts.begin()){
        i = contacts.end();
    }
    i--;
    nameLine->setText(i.key());
    addressText->setText(i.value());


}

void AddressBook::openListView(){


}
void AddressBook::updateInterface(Mode m){
   currentMode = m;
     size_t noOfContacts = contacts.size();
    switch(m){
    case NAV_MODE:{

        this->setEnabled(true);
        addBtn->show();
        addBtn->setEnabled(true);
        editBtn->show();
        editBtn->setEnabled(noOfContacts > 0);
        removeBtn->show();
        removeBtn->setEnabled(noOfContacts > 0);
        findBtn->show();
        findBtn->setEnabled(noOfContacts > 1);
        saveBtn->show();
        saveBtn->setEnabled(noOfContacts > 0);
        loadBtn->show();
        submitBtn->hide();
        cancelBtn->hide();
        nextBtn->show();
        nextBtn->setEnabled(noOfContacts > 1);
        prevBtn->show();
        prevBtn->setEnabled(noOfContacts > 1);
        nameLine->setReadOnly(true);
        addressText->setReadOnly(true);
        break;
    }
    case ADD_MODE:{

        this->setEnabled(true);
        addBtn->show();
        addBtn->setEnabled(false);
        editBtn->hide();
        removeBtn->hide();
        findBtn->hide();
        saveBtn->hide();
        loadBtn->hide();
        submitBtn->show();
        cancelBtn->show();
        nextBtn->hide();
        prevBtn->hide();
        nameLine->setReadOnly(false);
        addressText->setReadOnly(false);
        nameLine->setFocus(Qt::OtherFocusReason);
        break;
    }
    case EDIT_MODE:{

        this->setEnabled(true);

        addBtn->hide();
        editBtn->show();
        editBtn->setEnabled(false);
        removeBtn->hide();
        findBtn->hide();
        saveBtn->hide();
        loadBtn->hide();
        submitBtn->show();
        cancelBtn->show();
        nextBtn->hide();
        prevBtn->hide();
        nameLine->setReadOnly(false);
        addressText->setReadOnly(false);
        nameLine->setFocus(Qt::OtherFocusReason);
        break;
    }
    case DIALOG_OPEN:{
        this->setEnabled(false);
        break;
    }
    }
}

