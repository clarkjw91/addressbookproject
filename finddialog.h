#ifndef FINDDIALOG_H
#define FINDDIALOG_H
#include <QDialog>

class QLineEdit;
class QPushButton;
class FindDialog : public QDialog
{
    Q_OBJECT

public:
    FindDialog(QWidget * parent = 0);
    QString getFindText() const { return findText; };
public slots:
    void findBtnClicked();
private:
    QPushButton * findBtn;
    QLineEdit * lineEdit;
    QString findText;

};

#endif // FINDDIALOG_H
