#include "listview.h"
#include <QListWidget>
#include <QGridLayout>
#include <QMap>
#include <QModelIndex>
ListView::ListView(QMap<QString, QString> * contacts , QWidget *parent) : QWidget(parent), contacts(contacts)
{
    listView = new QListWidget(this);
    QGridLayout * mainLayout = new QGridLayout;
    mainLayout->addWidget(listView);
    setLayout(mainLayout);
    updateList();

    listView->show();

    connect(listView, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(itemDoubleClicked(QListWidgetItem*)));
}

void ListView::updateList(){
   listView->clear();
    for(auto i : contacts->keys()){
        listView->addItem(i);
    }

}

void ListView::setCurrentSelection(QString name){
    if(!contacts->contains(name)){
        return;
    }
    QList<QListWidgetItem*> cur = listView->findItems(name, Qt::MatchExactly);
   listView->setCurrentItem(cur.constFirst());

}
void ListView::itemDoubleClicked(QListWidgetItem* current){
    QString name = current->text();
    emit itemDoubleClicked(name);
}


QString ListView::getCurrentSelection(){
    QList<QListWidgetItem*> cur = listView->selectedItems();
    if(cur.length() < 1){
        return "";
    }
    return cur.constFirst()->text();

}
